#!/bin/bash

echo "No jardim da vida, as flores desabrocham"
sleep 2
echo "Em cores e perfumes que o tempo n~ao apaga."
sleep 2
echo "Cada dia 'e um verso, uma p'agina em branco," 
sleep 2
echo "Onde escrevemos hist'orias com cada passo que damos."
sleep 2
echo "As l'agrimas sao versos tristes, as alegrias sao rimas,"
sleep 2
echo "E no poema da ecistencia, cada  um tem seu papel."
sleep 2
echo "As vezes, as poesia e suave como uma brisa,"
sleep 2
echo "Outras, 'e tempestuosa como um mar agitado." 
sleep 2
echo "Mas sempre ha beleza na danca das palavras,"
sleep 2
echo "Na melodia da vida, que nunca deixa de tocar."
 
