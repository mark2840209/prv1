#!/bin/bash


echo "=== Informacoes da CPU=="

cat /proc/cpuinfo | grep "model name" | uniq
cat /proc/cpuinfo | grep "cpu MHz" | uniq

echo "=== Infor memory ==="
cat /proc/meminfo | grep "MemTotal"


echo "=== Infro Disc==="
df -h

echo "=== Info Placa VD ==="
lspci | grep VGA | cut -d ":" -f 3-

echo "===Info PlacaMae==="
dmidecode -t bios | grep "Vendor\|Version"
