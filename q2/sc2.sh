#!/bin/bash

read -p "Dir 1: " d1
read -p "Dir 2: " d2
read -p "Dir 3: " d3

ls -1 "$d1" | grep -c ".xls"
ls -1 "$d2" | grep -c ".xls"
ls -1 "$d3" | grep -c ".xls"

ls -1 "$d1" | grep -c ".bmp"
ls -1 "$d2" | grep -c ".bmp" 
ls -1 "$d3" | grep -c ".bmp"

ls -1 "$d1" | grep -c ".docx"
ls -1 "$d2" | grep -c ".docx"
ls -1 "$d3" | grep -c ".docx" 
